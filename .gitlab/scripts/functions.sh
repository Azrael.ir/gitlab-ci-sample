#!/bin/bash
# functions.sh: common functions

# die: print message and exit
# $1: message
# $2: exit code
die() {
  local message="$1"
  local retval="$2"
  [ -z "$retval" ] && retval=1
  echo "$message"
  exit "$retval"
}

# to deploy local folder on remote site
deploy_project() {
  local remote_user="$1"
  local remote_site="$2"
  local remote_dest_dir="$3"
  local remote_src_dir="$4"
  # checks
  [ -z "$remote_user" ] && die 'remote user not specified!'
  [ -z "$remote_site" ] && die 'remote site not specified!'
  [ -z "$remote_dest_dir" ] && die 'remote destination directory not specified!'
  [ -z "$remote_src_dir" ] && die 'remote source directory not specified!'
  # deploy to site
  if [ "$DEPLOY_MODE" = 'rsync-over-ssh' ]; then
    ssh -o StrictHostKeyChecking=no "${remote_user}@${remote_site}" "mkdir -p ${remote_dest_dir}"
  else
    mkdir -p "${remote_dest_dir}"
  fi
  # default is over ssh
  local rsync_ssh_deploy_opts=(-e 'ssh -o StrictHostKeyChecking=no -q')
  local rsync_remote_dest_dir="${remote_user}@${remote_site}:${remote_dest_dir}"
  if [ "$DEPLOY_MODE" = 'rsync' ]; then
    rsync_ssh_deploy_opts=()
    rsync_remote_dest_dir="${remote_dest_dir}"
  fi
  # https://www.digitalocean.com/community/tutorials/how-to-copy-files-with-rsync-over-ssh
  # TODO: add link to explain the rsync options used below
  rsync -v -rlpt \
    --exclude='.git' --exclude='.gitignore' --exclude='.gitlab' --exclude='.gitlab-ci.yml' \
    --exclude-from='.gitlab/files/deploy-exclude-list' --omit-dir-times \
    --no-perms --chmod=ugo=rwX --executability \
    "${rsync_ssh_deploy_opts[@]}" $RSYNC_DEBUG_PARAMS \
    "${remote_src_dir}/" "${rsync_remote_dest_dir}"
  if [ $? -eq 0 ]; then
    echo "deployed to $remote_dest_dir@$remote_site"
  fi
}
